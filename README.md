# fullstackagile.eu website

    npm install
    npm start

[Markdown reference](https://markdown-it.github.io/)

## Deployment configuration

* [AWS](./aws-setup.md)
* [Cloudflare](./cloudflare-setup.md)

