---
layout: page-en
permalink: "/"
title: Home
---

<img src="./logo-full-stack-agile.png" width="38%"/>

Full Stack Agile is an Agile consulting company.
What sets us apart is that
we consult in the full technology and organizational "stack" of the Agile approach:
from automated tests and planned refactoring,
to contionuous delivery and team autonomy,
to clear prioritization and ownership of decisions.

At all levels we encourage transparency and face-to-face conversations.
We facilitate stepwise improvements using the Agile toolbox
based on our client's specific situation and current needs. We consult in the Copenhagen region and also in the rest of Europe and the USA.

Check out our profiles and contact details:

<style>
table, tr, td { border: none; }
tr:nth-child(2n) { background-color: transparent; }
</style>
<table width="100%">

<tr>

<td width="50%">
    <div align="center">
        <h3>
            <a href="https://sju.xpqf.com/profile/" target="_blank">
                Sju G. Thorup
            </a>
        </h3>
        <p>M.Sc, MBA, strategy, leadership, project management</p>
    </div>
</td>

<td width="50%">
    <div align="center">
        <h3>
            <a href="/larsthorup">
                Lars Thorup
            </a>
        </h3>
        <p>Architecture, coding, testing, tools, DevOps</p>
    </div>
</td>

</tr>

<tr>

<td>
    <a href="https://sju.xpqf.com/profile/" target="_blank">
        <img src="./sju.jpg" />
    </a>
</td>

<td>
    <a href="https://stackoverflow.com/cv/larsthorup" target="_blank">
        <img src="./lars.jpg" />
    </a>
</td>

</tr>

</table>

Read our latest articles on team collaboration and software development [on our blog](/blog/).
