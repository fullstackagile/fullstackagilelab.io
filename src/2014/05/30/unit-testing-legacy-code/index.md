---
date: 2014-05-30
layout: post-en
tags: post
title: Unit testing legacy code
author: Lars
categories: [talk, testing]
permahost: "https://zealake.com"
permalink: "/2014/05/30/unit-testing-legacy-code/"
---

A couple of weeks back I gave a presentation for one of my clients on how to introduce unit testing into a large code base. Here is the appetizer:

> Unit testing and test-driven development are practices that makes it easy and efficient to create well-structured and well-working code. However, many software projects didn’t create unit tests from the beginning.
>
> In this presentation I will show a test automation strategy that works well for legacy code, and how to implement such a strategy on a project. The strategy focuses on characterization tests and refactoring, and the slides contain a detailed example of how to carry through a major refactoring in many tiny steps

And here are the [slides](https://www.slideshare.net/larsthorup/unit-testlegacycode).
