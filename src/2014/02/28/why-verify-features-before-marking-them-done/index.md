---
date: 2014-02-28
layout: post-en
tags: post
title: Why verify features before marking them "Done"?
author: Lars
categories: [thoughts, work process]
permahost: "https://zealake.com"
permalink: "/2014/02/28/why-verify-features-before-marking-them-done/"
---

On our backlog, we have the usual columns: Planned, In Progress and Done. But before the Done column we have an extra column called Verify. Our backlog looks like this:

![](./backlog-768x433.png)

When a feature has been fully developed and tested, we move it to Verify where the owner of that feature can verify that the feature works as they intended. This step typically catches misunderstandings in communication of intended behavior, rather than coding bugs. The owner then moves the feature to Done or back to In Progress depending on the verification outcome.

Feature verification is a useful feedback mechanism. When a feature is verified we might find important issues that need to be fixed (feedback to developers). When features get implemented but are slow to getting verified and used, maybe that feature wasn't as important as thought (feedback to product management). Or it shows that we are lacking feature owner resources compared to development resources (feedback to project management).

Feedback is always an opportunity for us to improve.
