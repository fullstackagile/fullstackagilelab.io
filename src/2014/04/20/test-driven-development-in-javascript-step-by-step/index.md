---
date: 2014-04-20
layout: post-en
tags: post
title: Test-Driven Development in JavaScript - step by step
author: Lars
categories: [talk, guide, testing]
permahost: "https://zealake.com"
permalink: "/2014/04/20/test-driven-development-in-javascript-step-by-step/"
---

I gave a talk together with [Rob Myers](http://agileinstitute.com/) at the Mile High Agile conference in Denver the other day. Here is the appetizer:

> Test-Driven Development (TDD) and Continuous Integration (CI) are far more common for back-end projects than they are for front-end JavaScript projects. But front-end projects benefit just as much from the rapid feedback of TDD+CI, and we demonstrate how easy it is to do using open source tools. We demonstrate how to write tests(-first) with Jasmine on a simple JavaScript application, including DOM manipulation and Ajax. Then we run the tests from the command line using GruntJS and PhantomJS, and finally set up continuous integration with Travis-CI.
> 
> Live demo! No slides, no existing code, plenty of audience
participation.
> 
> A step-by-step handout will be provided so participants can try the demo themselves after the session.

You can find the [handout](http://htmlpreview.github.io/?https://github.com/larsthorup/weather-tddjs/blob/master/steps.html) and the [complete code](https://github.com/larsthorup/weather-tddjs/) on GitHub.
