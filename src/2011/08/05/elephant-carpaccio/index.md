---
date: 2011-08-05
layout: post-en
tags: post
title: Elephant Carpaccio
author: Lars
categories: [talk, work process]
permahost: "https://zealake.com"
permalink: "/2011/08/05/elephant-carpaccio/"
---

For agile development to work well, it is important to have many small stories and many small tasks. Alistair Cockburn has coined the evocative term [elephant carpaccio](http://alistair.cockburn.us/Elephant+carpaccio) for the process of dividing epics into minimal achievable stories and decomposing stories into minimal achievable tasks. I have previously [blogged on this](/2010/09/03/decompose/) and yesterday I gave a presentation on this subject for a client. 

You can read the slides here:

<a title="Elephant Carpaccio" href="http://www.slideshare.net/larsthorup/elephant-carpaccio">

![](./elephant-carpaccio-1-728.jpg)

</a>
