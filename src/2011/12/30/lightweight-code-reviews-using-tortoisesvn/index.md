---
date: 2011-12-30
layout: post-en
tags: post
title: Lightweight code reviews using TortoiseSVN
author: Lars
categories: [tools, collaboration]
permahost: "https://zealake.com"
permalink: "/2011/12/30/lightweight-code-reviews-using-tortoisesvn/"
---

I believe that it is often a good idea to have another set of eyes looking at the code I write. The questions and comments resulting from such a code review indicate its value:

* "It seems like you are missing a test case for this scenario, and I think the code will break on it"
* "We already have some code elsewhere that implements this functionality; please reuse that"
* "That was a nice way to implement this function. I can use that elsewhere"
* "Now I learned about this new feature of CSS"

Code reviews can be done in many ways. Some prefer pair programming: review the code as it is being written, while others prefer to work alone writing the code and then having someone review it afterwards. Some companies have a policy mandating code reviews before the code is comitted, while others others prefer to increase the flow by allowing code reviews after commit. A discussion of the pros and cons of these different approaches in different situations is worth a seperate post.

Lately I have wanted to do code reviews after commits on one of our projects. To make it as easy as possible to manage the code reviews, I have looked for a tool that allowed me to easily mark commits with the name of a reviewer, and easily get a list of commits not yet marked as reviewed. GitHub has built-in support for [code reviews of pull requests](https://github.com/features/code-review). However I needed a solution that worked with our SVN repository. Several dedicated code review tools exist, but I was discouraged by the complexity added by those tools.

Then I found a simple solution utilizing a feature in TortoiseSVN that was made for [integration with issue trackers](http://tortoisesvn.net/docs/release/TortoiseSVN_en/tsvn-dug-bugtracker.html). Now I can use the Log window in TortoiseSVN to see which commits have been reviewed by whom. Here is a sample screenshot:

![](tortoisesvn-code-review-list-reviewer.png)

I can search for commits that have not yet been reviewed:

![](tortoisesvn-code-review-list-unreviewed.png)

If the code was written by a pair, the name of your partner can be written directly in the commit dialog, see this sample screenshot:

![](tortoisesvn-code-review-commit.png)

If the code is reviewed some time after the commit, the commit can be marked as reviewed by adding the review information in the Edit Log Message dialog:

![](tortoisesvn-code-review-reviewed.png)

To configure TortoiseSVN to behave in this way, all you have to do is to commit the following revision properties on the root folder of your project:

![](tortoisesvn-code-review-config.png)

Thanks to the TortoiseSVN team for making such a great versatile product!
