---
date: 2011-07-28
layout: post-en
tags: post
title: Always use the right tool for the job?
author: Lars
categories: [thoughts, tools]
permahost: "https://zealake.com"
permalink: "/2011/07/28/always-use-the-right-tool-for-the-job/"
---

Why are most software projects using a lot of different programming languages (e.g. SQL, Ruby, JavaScript, HTML, CSS)? Couldn't we just use a single one? Wouldn't that simplify everything?

The issue is only partly a language issue. Doing everything (UI, business logic, data access) in C++, as I have often done for embedded projects doesn't necessarily improve the situation. An application performs different tasks and different languages are better suited to different tasks, you know: using the right tool for the job often makes good sense. So HTML is good for presentation, CSS is good for styling, SQL is good for querying data, JavaScript, C#, Ruby, etc is good for business logic. The biggest problem is that developers need to be competent in all these different tasks, and only partly that different languages are used for these tasks.

This issue definitely impacts team structure. Most people tend to be stronger in some "languages" than in others. Collective code ownership becomes harder, the more "languages" are involved.

On a more technical level, another big issue is the so-called impedance mismatch between different data topologies used for different tasks. The relational structure of SQL and the hierarchical structure of XML and JSON and the network structure of objects in memory does not fit well with each other, so a lot of complicated mapping and conversion is necessary. This impacts the cost, both computationally and on developer resources.

There seems to be a trend towards standardizing on a single data representation across the different layers to minimize the impedance mismatch. For example JSON can be used for data representation throughtout an application using MongoDB as the database, node.js as the backend and JavaScript on the front end. This simplifies a lot of things and is becoming increasingly popular, but has drawbacks as well, like the lack of a proper SQL query language.

On a tool level, having different languages also impacts testing and debugging, because the tools and techniques for automated testing and debugging are different for each language.

Would you prefer to minimize the number of languages used on a project to increase collective code ownership and simplify testing and debugging? Or would you prefer to use the right tool for each job and then accept the costs of mapping, decreased collective code ownership and more complex testing and debugging setups?

What are the factors that guide us in this trade-off?
