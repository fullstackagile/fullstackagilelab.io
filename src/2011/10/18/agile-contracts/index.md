---
date: 2011-10-18
layout: post-en
tags: post
title: Agile Contracts
author: Lars
categories: [talk, business]
permahost: "https://zealake.com"
permalink: "/2011/10/18/agile-contracts/"
---

How do you sell an agile project? Most clients expect to buy software by time-and-material or by fixed-price-fixed scope contracts based on detailed requirements. These models cannot create a fertile environment for collaboration between client and vendor.

We have touched upon this [subject](/2010/09/26/pay-less-for-your-agile-project/) before, and today I am presenting on the topic for Bay Area Agile Leadership Network. In this presentation, I report on our experiments with commercial contracts that supports an agile development process, based on concrete examples of win-win contract types. I outline the different aspects of these contracts, as well as experiences creating and delivering software solutions under these contracts.

Slides are here:

<a href="http://www.slideshare.net/larsthorup/agile-contracts" title="Agile Contracts" target="_blank">

![](./agile-contracts-1-728.jpg)

</a>
