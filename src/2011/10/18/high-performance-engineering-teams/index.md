---
date: 2011-10-18
layout: post-en
tags: post
title: High Performance Engineering Teams
author: Lars
categories: [talk, guide]
permahost: "https://zealake.com"
permalink: "/2011/10/18/high-performance-engineering-teams/"
---

You want to shorten the time from *idea* to *live*. You want your team to develop faster. You want higher quality. In short: you want a high performance engineering team.

This presentation is based on my experiences building high performance engineering teams, and focuses on the technical practices required. These practices centers around automation (build, test and deployment) and increased collaboration between Engineering and QA (TDD, exploratory testing, prioritization, feedback cycles).

Slides are here:

<a href="http://www.slideshare.net/larsthorup/high-performance-software-engineering-teams" title="High Performance Software Engineering Teams" target="_blank">

![](./high-performance-software-engineering-teams-1-728.jpg)

</a>
