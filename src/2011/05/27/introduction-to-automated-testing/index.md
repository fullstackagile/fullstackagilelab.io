---
date: 2011-05-27
layout: post-en
tags: post
title: Introduction to Automated Testing
author: Lars
categories: [talk, testing]
permahost: "https://zealake.com"
permalink: "/2011/05/27/introduction-to-automated-testing/"
---

Yesterday I gave the first presentation for the [Test Driven Developers meetup group](http://www.meetup.com/Test-Driven-Developers-Bay-Area/) with an Introduction to Automated Testing (slides below). There were 16 enthusiastic participants and we had a very interactive and profitable night. Looking forward to our next event on June 14th on [Unit tests for your JavaScript application with MVC and QUnit](http://www.meetup.com/Test-Driven-Developers-Bay-Area/events/17470207/).

<a title="Introduction to Automated Testing" href="http://www.slideshare.net/larsthorup/introduction-toautomatedtesting">

![](./introduction-to-automated-testing-1-728.jpg)

</a>
