---
date: 2011-06-22
layout: post-en
tags: post
title: Automated Testing for Embedded Software in C or C++
author: Lars
categories: [testing, embedded]
permahost: "https://zealake.com"
permalink: "/2011/06/22/automated-testing-for-embedded-software-in-c-or-c/"
---

When software developers write automated tests for their software, the quality increases, the design improves and the project becomes more manageable. The development also becomes more fun!

In this presentation (slides below) you will learn how to write automated tests for embedded software. You will see a live demonstration of writing an automated test for a feedback control algorithm in C. I will also talk about the benefits of writing tests and why it can actually improve your design and save you time.

When having a large set of automated tests it becomes valuable to run all tests automatically every time the code is changed. I will touch upon what is the best continuous integration setup for embedded software projects.

Slides are here:

<a title="Automated Testing for Embedded Software in C or C++" href="http://www.slideshare.net/larsthorup/automated-testing-for-embedded-software-in-c-or-c">

![](./automated-testing-for-embedded-software-in-c-or-c-1-728.jpg)

</a>
