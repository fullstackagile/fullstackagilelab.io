---
date: 2011-06-15
layout: post-en
tags: post
title: Unit Testing in JavaScript using MVC and QUnit
author: Lars
categories: [testing, UI]
permahost: "https://zealake.com"
permalink: "/2011/06/15/unit-testing-in-javascript-using-mvc-and-qunit/"
---

I gave another presentation at the [Test Driven Developers meetup group](http://www.meetup.com/Test-Driven-Developers-Bay-Area/) on Unit Testing in JavaScript using MVC and QUnit (slides below). 

Looking forward to our next event on June 23rd, a [TDD Coding Dojo](http://www.meetup.com/Test-Driven-Developers-Bay-Area/events/22316421/) hosted by Olve Maudal.

<a title="Unit Testing in JavaScript with MVC and QUnit" href="http://www.slideshare.net/larsthorup/unit-testing-in-javascript-with-mvc-and-qunit">

![](./unit-testing-in-javascript-with-mvc-and-qunit-1-728.jpg)

</a>
