---
date: 2012-12-25
layout: post-en
tags: post
title: Check your JavaScript code on every save
author: Lars
categories: [KPIs]
permahost: "https://zealake.com"
permalink: "/2012/12/25/check-your-javascript-code-on-every-save/"
---

Being able to [check the quality](/2012/12/25/automated-build-for-your-front-end-javascript-code/) of your code and [run all unit tests](/2012/12/25/run-all-your-javascript-qunit-tests-on-every-commit/) in your project [on every commit](/2012/12/25/continuous-integration-of-your-front-end-javascript-code-using-travis-ci/) is useful. If you could do it on every save you would get even faster feedback.

Grunt includes a *watch* facility that will run a task every time some file is changed. You only need to tell Grunt which files to watch and what tasks to run, so change your Gruntfile.js to include:
``` js
    grunt.loadNpmTasks('grunt-contrib-watch');
    gruntConfig.watch = {
        scripts: {
            files: ['src/**/*.*'],
            tasks: ['lint', 'test']
        }
    };
```

You can now open a new terminal window and run
``` shell-session
$ grunt watch
```

Then use your editor and edit index.html to intentionally break the test. Save the file, and watch the immediate feedback in the adjacent terminal window. Fix the test, save the file, and see that everything is back in order. You can't get feedback much faster than that. Enjoy!

This post is part of a series on Continuous Integration for front-end JavaScript, read the other posts in the series:

* [Automated build for your front-end JavaScript code](/2012/12/25/automated-build-for-your-front-end-javascript-code/)
* [Continuous Integration of your front-end JavaScript code using Travis-CI](/2012/12/25/continuous-integration-of-your-front-end-javascript-code-using-travis-ci/)
* [Run all your JavaScript QUnit tests on every commit](/2012/12/25/run-all-your-javascript-qunit-tests-on-every-commit/)
* Check your JavaScript code on every save
* [No line of your JavaScript code uncovered](/2012/12/25/no-line-of-your-javascript-code-uncovered/)
* [Run all your JavaScript Jasmine tests on every commit](/2013/04/07/run-all-your-javascript-jasmine-tests-on-every-commit/)
