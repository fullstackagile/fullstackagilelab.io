---
date: 2012-12-25
layout: post-en
tags: post
title: Run all your JavaScript QUnit tests on every commit
author: Lars
categories: [pipeline]
permahost: "https://zealake.com"
permalink: "/2012/12/25/run-all-your-javascript-qunit-tests-on-every-commit/"
---

Being able to [check the quality](/2012/12/25/automated-build-for-your-front-end-javascript-code/) of your code [on every commit](/2012/12/25/continuous-integration-of-your-front-end-javascript-code-using-travis-ci/) is useful. But it would be even more useful if you could also run all your unit tests on every commit. Here I will show how you can extend the automated build setup to also run all your [QUnit](http://qunitjs.com/) tests. (Switch to different blog post if you are using the [Jasmine framework](/2013/04/07/run-all-your-javascript-jasmine-tests-on-every-commit/))

Assume that we have the following very simple QUnit test in `src/test/index.html`:

``` html
    <title>jsdevenv tests</title>
    <div id="qunit"></div>
    <script>
        runTests = function () {
            module('my module');
            test('my test', function () {
                equal(2 + 2, 4, 'kindergarten');
            });
        }
    </script>
```

Make sure to put qunit.css and qunit.js in src/test/lib. You can then run the test by opening index.html in a browser.

![](./jsdevenv-qunit.png)

[Grunt](/2012/12/25/automated-build-for-your-front-end-javascript-code/) has built-in support for running QUnit tests using [PhantomJS](http://phantomjs.org/) which is a headless browser (headless means that it runs from the command line without requiring a display to open a window on -- very useful for a continuous integration server)

You will need to tell Grunt how to run your QUnit tests. Change your Gruntfile.js to include:
``` js
    grunt.loadNpmTasks('grunt-contrib-qunit');
    gruntConfig.qunit = {
        src: ['src/test/index.html']
    };
    grunt.registerTask('test', 'qunit:src');
```
You can then run your unit tests from the command line with
``` shell-session
$ grunt test
```

To make sure that the tests will also be run by your continuous integration server, remember to adjust the travis alias you created in Gruntfile.js for that purpose:

``` js
grunt.registerTask('travis', ['lint', 'test']);
```

Commit and push to GitHub and watch Travis-CI run your unit tests. Then try making one of the tests fail and commit the change to see how your continuous integration server reports errors.

Some continuous integration servers (like Jenkins, TeamCity, and Bamboo, but not yet Travis-CI) can show detailed reports and graphs based on test results. To make this work you will need to ensure that the automated build produces one or more XML-files in JUnit format containing all the test results. There is a Grunt plugin, called grunt-junit, that extends the Grunt QUnit runner to produce JUnit XML files.

First step is to install the grunt-junit plugin, so it needs to be included in the list of development dependencies in package.json.

You will also need to tell Grunt that it should load the grunt-junit plugin. Add these lines to Gruntfile.js:

``` js
    grunt.loadNpmTasks('grunt-qunit-junit');
    gruntConfig.qunit_junit = {
        options: {
            dest: 'output/testresults'
        }
    };
```

and modify this line:

``` js
grunt.registerTask('test', ['qunit_junit', 'qunit:src']);
```

The `grunt-junit` plugin will store the JUnit XML files in the specified destination directory.

Now run

``` shell-session
$ grunt test
```

and verify the contents of the XML file that is produced in the `output/testresults` directory.

Finally, you will need to configure your continuous integration server to parse any XML-files in the `output/testresults` directory. Then you will be able to view detailed test results and graphs, like this trend line from TeamCity:

![](./pluto-qunit-teamcity.png)

Now, whenever you commit changes to your code, you will get an updated test report telling you immediately of any regressions that might have been introduced.

This post is part of a series on Continuous Integration for front-end JavaScript, read the other posts in the series:

* [Automated build for your front-end JavaScript code](/2012/12/25/automated-build-for-your-front-end-javascript-code/)
* [Continuous Integration of your front-end JavaScript code using Travis-CI](/2012/12/25/continuous-integration-of-your-front-end-javascript-code-using-travis-ci/)
* Run all your JavaScript QUnit tests on every commit
* [Check your JavaScript code on every save](/2012/12/25/check-your-javascript-code-on-every-save/)
* [No line of your JavaScript code uncovered](/2012/12/25/no-line-of-your-javascript-code-uncovered/)
* [Run all your JavaScript Jasmine tests on every commit](/2013/04/07/run-all-your-javascript-jasmine-tests-on-every-commit/)
