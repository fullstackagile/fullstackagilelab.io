---
date: 2012-12-25
layout: post-en
tags: post
title: Automated build for your front-end JavaScript code
author: Lars
categories: [pipeline]
permahost: "https://zealake.com"
permalink: "/2012/12/25/automated-build-for-your-front-end-javascript-code/"
---

A lot of projects use continuous integration these days. It is still far more common for back-end projects using C#, Java or Ruby than it is for JavaScript projects. But front-end projects using JavaScript can also benefit from continuous integration, and this series of blog posts will show you how easy it is to set up using available open source tools.

I will show how to do fully automated code quality analysis with JSHint, [unit testing](/2012/12/25/run-all-your-javascript-qunit-tests-on-every-commit/) with QUnit, [code coverage](/2012/12/25/no-line-of-your-javascript-code-uncovered/) with JSCoverage and [continuous integration](/2012/12/25/continuous-integration-of-your-front-end-javascript-code-using-travis-ci/) with Travis-CI.

Continuous integration means that your code will be checked automatically on every commit. If you commit often, you will get very precise information about what change introduced an error. Frequent and precise feedback is the cheapest way to keep bugs at bay.

All continuous integration systems rely on the ability to do the checking of the code from the command line. This blog post is about front-end JavaScript. While it is meant to be run in a browser, nonetheless there are great tools available for checking this front-end JavaScript code from the command line. Here I will focus on Node and Grunt. You can access the [sample code](http://github.com/larsthorup/jsdevenv) in these blog posts at GitHub.

You will need only a single tool being installed globally on your system, namely [Node.js](http://nodejs.org/). All other tools will be added locally to your project directory.

First step is to install Node.js on your system. Follow the [instructions](http://nodejs.org/).

Your first goal will be to check your JavaScript code quality using [JSHint](http://www.jshint.com/), a tool that detects errors and potential problems in JavaScript code and enforces coding conventions. To be able to check the code from the command line we will use [Grunt](http://gruntjs.com/), a command line build tool for JavaScript. Grunt has built-in support for JSHint.

You can use the Node Package Manager, npm, to install any other tools you will be using. Npm is configured using a file called `package.json` located in the root directory of your project. To let Node know that you want to use Grunt, you will need to add Grunt as a development dependency to package.json. I have included some standard Grunt plugins, that we will need later on:
``` json
{
  "name": "jsdevenv",
  "version": "0.1.1-1",
  "description": "A sample build environment for JavaScript",
  "author": "Lars Thorup ",
  "devDependencies": {
    "grunt-cli": "0.1.6",
    "grunt": "0.4.0",
    "grunt-contrib-jshint": "~0.1.1",
    "grunt-contrib-qunit": "~0.1.1",
    "grunt-qunit-junit": "~0.1.0",
    "grunt-contrib-watch": "~0.2.0",
    "grunt-qunit-cov": "~0.3.2"
  }
}
```

You can then install Grunt locally by running the following command in the root directory of your project (the directory with package.json):
``` shell-session
$ npm install
```

Next thing to do is to configure Grunt to check your code quality. Grunt needs a configuration file, and we will use `Gruntfile.js`. A basic Gruntfile.js that will check your code quality looks like this:

``` js
/*global module*/
module.exports = function (grunt) {
  'use strict';

  var gruntConfig = {};
  grunt.loadNpmTasks('grunt-contrib-jshint');
  gruntConfig.jshint = {
      options: { bitwise: true, camelcase: true, curly: true, eqeqeq: true, forin: true, immed: true,
          indent: 4, latedef: true, newcap: true, noarg: true, noempty: true, nonew: true, plusplus: true,
          quotmark: true, regexp: true, undef: true, unused: true, strict: true, trailing: true,
          maxparams: 3, maxdepth: 2, maxstatements: 50},
      all: [
          'Gruntfile.js',
          'src/js/**/*.js'
      ]
  };
  grunt.initConfig(gruntConfig);

};
```

Based on your style preferences you might want to [configure JSHint](http://www.jshint.com/docs/) differently than the example used here.

Note that `Gruntfile.js` is a JavaScript file, so it makes sense to include this file in the list of files to be checked. You may also need to tweak the second path of the array `gruntConfig.jshint.all` if your JavaScript files are located somewhere else in the project.

Now you can check the quality of your JavaScript code from the command line by running:

``` shell-session
$ grunt jshint
```

Next step would be to configure your continuous integration server to execute the following two steps on every commit to your project:

``` shell-session
$ npm install
$ grunt lint
```

How to do this will depend on what continuous integration software you are using. In the next blog post I will show [how to continuously integrate your JavaScript code](/2012/12/25/continuous-integration-of-your-front-end-javascript-code-using-travis-ci/) with Travis-CI.

This post is part of a series on Continuous Integration for front-end JavaScript, read the other posts in the series:

* Automated build for your front-end JavaScript code
* [Continuous Integration of your front-end JavaScript code using Travis-CI](/2012/12/25/continuous-integration-of-your-front-end-javascript-code-using-travis-ci/)
* [Run all your JavaScript QUnit tests on every commit](/2012/12/25/run-all-your-javascript-qunit-tests-on-every-commit/)
* [Check your JavaScript code on every save](/2012/12/25/check-your-javascript-code-on-every-save/)
* [No line of your JavaScript code uncovered](/2012/12/25/no-line-of-your-javascript-code-uncovered/)
* [Run all your JavaScript Jasmine tests on every commit](/2013/04/07/run-all-your-javascript-jasmine-tests-on-every-commit/)
