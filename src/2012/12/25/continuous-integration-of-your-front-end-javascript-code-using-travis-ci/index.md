---
date: 2012-12-25
layout: post-en
tags: post
title: Continuous Integration of your front-end JavaScript code using Travis-CI 
author: Lars
categories: [pipeline]
permahost: "https://zealake.com"
permalink: "/2012/12/25/continuous-integration-of-your-front-end-javascript-code-using-travis-ci/"
---

The previous post shows how to [execute build steps from the command line](/2012/12/25/automated-build-for-your-front-end-javascript-code/). Usually this is all that is required for setting up a continuous integration server that will run those steps on every commit to your project. Since the sample code behind these blog posts is available on [GitHub](http://github.com/larsthorup/jsdevenv), you can easily set up continuous integration for the code using the hosted [Travis-CI](http://travis-ci.org/) system.

First you need to make Travis-CI aware of who you are and what GitHub repositories you want to continuously integrate. Follow [step 1 and 2](http://about.travis-ci.org/docs/user/getting-started/) from the instructions.

Next step is to tell Travis-CI what platform your build system is running on. Here I use Node, which Travis-CI supports out of the box. You configure Travis-CI in a file in the root directory of your project, called `.travis.yml`. It should contain these instructions:

``` yaml
language: node_js
node_js:
  - 0.9
```

0.9 will be the version of Node that Travis-CI uses to run your builds.

For build systems based on Node, Travis-CI will run the build with the command

``` shell-session
$ npm test
```

To make that command execute the grunt command you need to extend the package.json file to look like this with the scripts section added:

``` json
{
  "name": "jsdevenv",
  ...
  "scripts": {
    "test": "grunt travis"
  }
}
```

Finally you can add the following line to Gruntfile.js to specify what grunt tasks you want Travis-CI to run:

``` js
grunt.registerTask('travis', 'jshint');
```

Commit your 3 files (package.json, .travis.yml and Gruntfile.js) to GitHub and check the status on the project-specific status page on Travis-CI. For the sample code behind these blog posts, the status page is [http://travis-ci.org/larsthorup/jsdevenv](http://travis-ci.org/larsthorup/jsdevenv).

Finally you can add an icon directly to your GitHub project page by adding the following markup to `README.md` (adjusted to reference your own project):

``` markdown
[![Build Status](https://travis-ci.org/larsthorup/jsdevenv.png)](https://travis-ci.org/larsthorup/jsdevenv)
```

Now every time you push new code to GitHub, your project page ([http://github.com/larsthorup/jsdevenv](http://github.com/larsthorup/jsdevenv)) will reflect if the automated build was successful or not, like this:

<a href="http://travis-ci.org/larsthorup/jsdevenv" target="_blank">

![](http://travis-ci.org/larsthorup/jsdevenv.png)


</a>

And you can click on the icon and see the detailed build output on Travis-CI. Pretty cool, ain't it?

This post is part of a series on Continuous Integration for front-end JavaScript, read the other posts in the series:

* [Automated build for your front-end JavaScript code](/2012/12/25/automated-build-for-your-front-end-javascript-code/)
* Continuous Integration of your front-end JavaScript code using Travis-CI
* [Run all your JavaScript QUnit tests on every commit](/2012/12/25/run-all-your-javascript-qunit-tests-on-every-commit/)
* [Check your JavaScript code on every save](/2012/12/25/check-your-javascript-code-on-every-save/)
* [No line of your JavaScript code uncovered](/2012/12/25/no-line-of-your-javascript-code-uncovered/)
* [Run all your JavaScript Jasmine tests on every commit](/2013/04/07/run-all-your-javascript-jasmine-tests-on-every-commit/)
