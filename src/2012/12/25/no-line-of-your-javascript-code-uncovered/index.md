---
date: 2012-12-25
layout: post-en
tags: post
title: No line of your JavaScript code uncovered
author: Lars
categories: [pipeline, KPIs]
permahost: "https://zealake.com"
permalink: "/2012/12/25/no-line-of-your-javascript-code-uncovered/"
---

Being able to [get fast feedback from failing unit tests](/2012/12/25/run-all-your-javascript-qunit-tests-on-every-commit/) is useful. And if you could easily identify the parts of your code that you still need to write tests for, you would get more comprehensive feedback.

There is a plugin for Grunt that can use the [JSCoverage](http://siliconforks.com/jscoverage/) tool to produce a coverage report while running your unit tests.

First step is to install JSCoverage. Unfortunately I have not been able to find a working npm package for JSCoverage, so what you can do is download a [binary](http://siliconforks.com/jscoverage/download.html) and put it somewhere in your path. The `grunt-qunit-cov` plugin also needs PhantomJS to be on the path, so you might need to install PhantomJS separately for that.

Grunt has a `grunt-qunit-cov` plugin that will combine the QUnit runner with JSCoverage. 

You will need to configure the plugin, so change your Gruntfile.js to include:
``` js
    grunt.loadNpmTasks('grunt-qunit-cov');
    gruntConfig['qunit-cov'] = {
        test:
        {
            minimum: 0.99,
            baseDir: 'src',
            srcDir: 'src/js',
            depDirs: ['src/lib', 'src/test'],
            outDir: 'output/coverage',
            testFiles: ['src/test/index.html']
        }
    };
    grunt.registerTask('coverage', 'qunit-cov');
```

In this example the plugin will fail the build if the coverage is not 99% or more.

To demonstrate the capabilities of JSCoverage, create an application file src/js/password.js:

``` js
/*global window*/
window.passwordStrength = function (password) {
    'use strict';
    var result = 0;
    if (password.length > 4) {
        result += 1;
    }
    if (password.length > 8) {
        result += 1;
    }
    return result;
};
```

Then add a test for it in src/test/index.html:

``` html
    <title>jsdevenv tests</title>
    <div id="qunit"></div>
    <script>    
        runTests = function() {
            module('password');
            test('strength is 0', function () {
                 equal(passwordStrength('abc'), 0, 'abc');
            });
        }
    </script>
```

Then invoke

``` shell-session
$ grunt coverage
```

This will fail with the following output:

``` shell-session
  Coverage in 75%
   Error: Coverage don't reach 99%</pre>
```

A detailed report is saved in `output/coverage/out/coverage.html`

![](./jsdevenv-coverage-failed.png)

And if you click on the link to `password.js`, you will see exactly which lines of `password.js` is not covered by any of the tests.

![](./jsdevenv-coverage-password-failed.png)

To fix the broken build, add the following two tests in `src/test/index.html`:

``` js
   test('strength is 1', function () {
     equal(passwordStrength('lemon'), 1, 'abc');
   });
   test('strength is 2', function () {
     equal(passwordStrength('lemonades'), 2, 'abc');
   });
```

Now `grunt coverage` will succeed with this message:

``` shell-session
  Coverage in 100%
```

And the coverage reports looks like this:

![](./jsdevenv-coverage-passed.png)

![](./jsdevenv-coverage-password-passed.png)

Most continuous integration systems (like Jenkins, TeamCity and Bamboo, but not yet Travis-CI) allow you to configure a set of artifacts that are made accessible for every build. See this screenshot from TeamCity:

![](./jsdevenv-coverage-teamcity.png)

Now you can make sure that you maintain a comprehensive test suite for your front-end JavaScript code. Enjoy!

This post is part of a series on Continuous Integration for front-end JavaScript, read the other posts in the series:

* [Automated build for your front-end JavaScript code](/2012/12/25/automated-build-for-your-front-end-javascript-code/)
* [Continuous Integration of your front-end JavaScript code using Travis-CI](/2012/12/25/continuous-integration-of-your-front-end-javascript-code-using-travis-ci/)
* [Run all your JavaScript QUnit tests on every commit](/2012/12/25/run-all-your-javascript-qunit-tests-on-every-commit/)
* [Check your JavaScript code on every save](/2012/12/25/check-your-javascript-code-on-every-save/)
* No line of your JavaScript code uncovered
* [Run all your JavaScript Jasmine tests on every commit](/2013/04/07/run-all-your-javascript-jasmine-tests-on-every-commit/)
