---
date: 2012-03-25
layout: post-en
tags: post
title: Test and Behavior Driven Development (TDD/BDD)
author: Lars
categories: [talk, testing]
permahost: "https://zealake.com"
permalink: "/2012/03/25/test-and-behavior-driven-development-tddbdd/"
---

This week I gave a presentation for a client, starting on Test and Behavior Driven Development.

In this introduction we give a high level description of what it is and why it is useful for developers. Then we go into some details on stubs and mocks, test data, UI testing, SQL testing, JavaScript testing, web services testing and how to start doing TDD/BDD on an existing code base.

<a title="Test and Behaviour Driven Development (TDD/BDD)" href="http://www.slideshare.net/larsthorup/test-and-behaviour-driven-development-tddbdd">

![](./test-and-behaviour-driven-development-tddbdd-1-728.jpg)

</a>
