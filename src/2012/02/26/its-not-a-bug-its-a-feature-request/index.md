---
date: 2012-02-26
layout: post-en
tags: post
title: It's not a bug, it's a feature request
author: Lars
categories: [thoughts, work process]
permahost: "https://zealake.com"
permalink: "/2012/02/26/its-not-a-bug-its-a-feature-request/"
---

I previously advised that [you shouldn't track bugs](/2010/08/22/dont-track-bugs/) but rather fix them immediately.  But not every issue reported as a bug should be treated as a bug in this sense. So how do we distinguish between a bug and a feature request?

Issues are reported when the software is not behaving as expected. That expectation should then be qualified and questioned in order to determine how well-founded it is. Obviously, an expectation arising from one user's momentary misunderstanding should not be considered a bug.

If the expectation is based on facts such as:

* The software recently worked this way
* The manual or the contract or the specification says so, and that part of the software was thought to be implemented already

or on "almost"-facts such as:

* Even though no specification explicitly says so, it can easily be argued to be "obvious" that the software should behave this way.

then I consider the expectation well-founded, and the current software behavior is a bug. The software development team will put it topmost on the task board, prioritize it higher than any other feature or improvement, and fix it first thing.

For an example, our product has a feature of exporting a full data set from one server and later importing the data set on another server. Recently an issue was reported where a particular data set had been exported but failed to import. In my mind, this is an example of an undoubted bug. The particularity of the failing data set was not covered by any specification, but it can be reasonably claimed that the software should be able to import this data set; so the code was broken and had to be fixed immediately. The software development team should make this call and not ask the product owner to prioritize.

Some expectations are not as fact-based or obvious but even so can be considered highly reasonable. If the software confuses every user, and undue effort is spent to explain and learn the unexpected behavior, there is good reason to give the item high priority.

For example, our product allows users to see comments from other users and each user has a distinct category. Recently an issue was reported because category A users could see comments from category B users which should not be the case.

However, I will argue that this issue is not a bug. The software does work, however confusingly or annoyingly. No specification has mentioned this issue, and I don't consider it obvious that the software should behave in this way. In a case like this it becomes the product owner's call how to prioritize the fixing of this type of unexpected behavior. The question of "undue effort to explain software behavior" is one of the important aspects that the product owner must consider when continuously prioritizing the development tasks.

As always, I recommend everybody to think for themselves. You can't get excellent results by just following rules, such as exactly how many users are impacted or how many seconds of downtime is implied. The best possible judgment comes from combining rules of thumb with your own knowledge, experience and intellect -- and the second opinion of a good coworker.
