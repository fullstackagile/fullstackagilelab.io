---
date: 2012-09-11
layout: post-en
tags: post
title: Tool Improvement Spikes
author: Lars
categories: [thoughts, tools]
permahost: "https://zealake.com"
permalink: "/2012/09/11/tool-improvement-spikes/"
---

A client of mine asked me this question today:

> One of our developers wants to write her BDD features using SpecFlow instead of our existing Cucumber JVM approach. Isn't it a bad idea having two separate approaches? What are your thoughts on this.

I think it is important that you constantly ask youself how you can improve on existing practices. Conducting experiments with a new tool that seems to have benefits over an existing tool is a great way to spike a potential improvement. You are not going to keep the same tool chain over the next 10 years anyway, so better do spikes early than wait until you are forced to change by outside forces.

I am not sure what the benefits might be in this particular case, but from a quick look, it seems like SpecFlow might be better integrated with the rest of the tool chain (.NET, Visual Studio). However, there might be drawbacks as well, such as integration with Jenkins reporting.

I don't think you should accept that one of the developers (let's call her Sandra) uses another tool just because she wants to. I also don't think you should encourage having two separate approaches to solving the same problem.  But you can leverage Sandra's desire to improve by asking her to run a few experiements to demonstrate answers to some actual questions you can pose in advance, like

* how well does it integrate with Visual Studio in day-to-day work?
* how easy is it to integrate with the existing build scripts and Jenkins reporting?
* how easy is it to write and debug compared to Cucumber?

Then Sandra can do a demo for the team and you can all discuss pros and cons on the basis of her results.

If the team agrees that Cucumber is best, Sandra should throw out her SpecFlow experiments and use Cucumber like the rest of  the team. But when the majority of the team agrees that some new tool seems to be better than the existing one, you can gradually discourage the use of the old tool and educate and encourage the use of the new one.
