---
date: 2012-05-11
layout: post-en
tags: post
title: Automated Performance Testing
author: Lars
categories: [testing, performance]
permahost: "https://zealake.com"
permalink: "/2012/05/11/automated-performance-testing/"
---

If you are like most test driven developers, you write automated tests for your software to get fast feedback about potential problems. Most of the tests you write will verify the functional behaviour of the software: When we call this function or press this button, the expected result is that value or that message.

But what about the non-functional behaviour, such as performance: When we perform this query the expected speed of getting results should be no more than that many milliseconds. It is important to be able to write automated performance tests as well, because they can give us early feedback about potential performance problems. But expected performance is not as clear-cut as expected results. Expected results are either correct or wrong. Expected performance is more like a threshold: If the performance is worse than this, we want the test to fail.

At yesterdays [meetup](http://www.meetup.com/Test-Driven-Developers-Bay-Area/events/59331662/) I gave the following presentation.

<a href="http://www.slideshare.net/larsthorup/automated-performance-testing" title="Automated Performance Testing" target="_blank">

![](./automated-performance-testing-1-728.jpg)

</a>
