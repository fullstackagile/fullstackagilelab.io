---
date: 2015-02-21
layout: post-en
tags: post
title: Going mobile - expand existing app or write a new one?
author: Lars
categories: [thoughts, architecture]
permahost: "https://zealake.com"
permalink: "/2015/02/21/going-mobile-expand-existing-app-or-write-a-new-one/"
---

One of my friends wrote to me the other day with an interesting architectural question, which I'll paraphrase here:

> What are your thoughts on running your client-side web apps in a mobile browser if you need to do more than just responsive design concepts? For instance if we want to limit some functionality for mobile browsers only. *Some people are suggesting write a new app from scratch but still utilize our REST services.  I would be inclined to do this if we knew mobile only needed to be a small subset of our functionality and that would never change. However I foresee that we would have to make changes in multiple apps too frequently.*
>
>I am leaning more towards overhauling the HTML/CSS to be responsive, expanding the configuration so we can turn off features for mobile and switching out views/templates. This way we can hopefully build in one spot and take advantage of existing code but make it more mobile browser friendly.

In short, I agree with him, but this was a nice chance to think a bit about why I believe so.

Many years working in engineering has taught me that many people love to start new projects. Usually this means that the new product will be nice and fancy and shiny (for a while) but will also lack features for a long time. At the same time, having to spend your engineering resources on two different code bases will often starve the old project for resources and that project will therefore stall. Users will end up having to choose between the new product and miss out on features, or stay on the old product and miss out on bug fixes and new features. They might end up leaving for a competitor that does not try to do two things at the same time.

On the other hand you do sometimes have to abandon old products, especially when the technology used is no longer maintained or working well or the code base is in a particularly bad state. The shift from back-end-heavy apps to front-end-heavy apps is an example of that.

So it is a trade-off. How bad is the existing code base for the new platforms we want to target, versus how fast are users abandoning the old platforms.

But for mobile web-apps, I don't see that trade-off. All users on all browsers on all platforms will benefit from a more responsive UI, and from the smoother workflows that you strive for with mobile. Giving the user the new smooth workflow as default, while also allowing the user to switch to the original fully featured workflow will be great improvement for any product. So, assuming the current code base is in good shape, I believe you should implement those improvements in your current code base, as refactorings and new features, and make them available on both mobile and desktops.

Be prepared for a bit of resistance from new developers on the team, who might have dreamed of starting a new project. Find other ways to make them happy.
