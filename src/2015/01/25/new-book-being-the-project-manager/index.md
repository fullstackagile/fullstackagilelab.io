---
date: 2015-01-25
layout: post-en
tags: post
title: New book - Being the project manager
author: Sju
categories: [announcement, work process]
permahost: "https://zealake.com"
permalink: "/2015/01/25/new-book-being-the-project-manager/"
---

<a href="http://www.lulu.com/shop/search.ep?keyWords=sju+thorup" target="_blank">

![](./Being-the-project-manager-cover-292x300.jpg)

</a>

This week [Sju G. Thorup](https://www.linkedin.com/in/sjuthorup)'s book “Being the project manager” was published. ZeaLake supported Sju's work, and I am very happy with the result.

“Being the project manager” is written for new project managers and people who aspire to lead a project. It is not a traditional textbook, and it is suited for Agile and traditional project management approaches alike. The book is intended as a complement for any basic training course or textbook about a project management methodology.

You get a tour into the many non-method-specific disciplines you need to master: communicating with stakeholders, motivate people, be open to new information, mitigate risks, and make decisions. The book offers fresh angles on the variety of project life and on your own style as a project manager. It contains a multitude of content: short chapters, inspiring stories from real project managers, a few simple self-tests -- and I especially enjoy the lively cartoons.

Sju writes in a straightforward style, and the book is an easy read. The illustrations definitely appear to advantage in the printed version, but the book is also available as a more affordable eBook.

Both editions are already available on [Lulu](http://www.lulu.com/shop/search.ep?keyWords=sju+thorup) and the eBook is on [iBookstore](http://itunes.apple.com/us/book/isbn9781312842168), and will soon be available on [Amazon](https://www.amazon.com/Being-Project-Manager-Sju-Thorup-ebook/dp/B00TG0KSJI/) as well.

Below is a wonderful sample cartoon from the book (click to enlarge).

<a href="./Action-cartoon-from-Being-the-project-manager-copyright-Sju-G-Thorup.png" target="_blank">

![](./Action-cartoon-from-Being-the-project-manager-copyright-Sju-G-Thorup-300x251.png)

</a>
