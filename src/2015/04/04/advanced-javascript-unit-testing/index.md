---
date: 2015-04-04
layout: post-en
tags: post
title: Advanced JavaScript Unit Testing
author: Lars
categories: [talk, testing]
permahost: "https://zealake.com"
permalink: "/2015/04/04/advanced-javascript-unit-testing/"
---

I gave a talk yesterday at Mile High Agile 2015:

> A lot of applications these days have a substantial, if not a major, part written in JavaScript. And not only for the front-end part, as Node.js is gaining popularity on the back-end. You might already have started doing some unit testing for your JavaScript code, but JavaScript has a quite a few concepts where it differs from traditional back-end programming languages like C# or Ruby. This fast-paced talk will show best practices for unit testing code involving 7 of those concepts. We will cover:
>
> * Asynchronous code, both with callbacks and with promises
> * Time and timers
> * Ajax requests
> * DOM manipulation
> * Responsive design with CSS media queries
> * Cross browser compatibility
> * Leak detection
> 
> The talk will include a number of live demonstrations covering JavaScript in the browser and Node.js. The demos will use different testing frameworks, including Mocha, Sinon, Jasmine and Karma.</blockquote>

Here are the slides:

<a href="//www.slideshare.net/larsthorup/advanced-javascript-unittesting" title="Advanced Javascript Unit Testing" target="_blank">

![](./advanced-javascript-unit-testing-1-638.jpg)

</a>
