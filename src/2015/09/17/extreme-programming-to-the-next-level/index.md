---
date: 2015-09-17
layout: post-en
tags: post
title: Extreme Programming - to the next level
author: Lars
categories: [talk, thoughts]
permahost: "https://zealake.com"
permalink: "/2015/09/17/extreme-programming-to-the-next-level/"
---

Inspired by a blog post by Benji Weber on [Modern Extreme Programming](http://benjiweber.co.uk/blog/2015/04/17/modern-extreme-programming/), tonight I am giving a presentation on these ideas. Find my slides below:

<a href="//www.slideshare.net/larsthorup/extreme-programming-to-the-nextlevel" title="Extreme Programming - to the next-level" target="_blank">

![](./extreme-programming-to-the-nextlevel-1-638.jpg)

</a>
