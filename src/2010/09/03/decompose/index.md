---
date: 2010-09-03
layout: post-en
tags: post
title: Decompose!
author: Lars
categories: [work process]
permahost: "https://zealake.com"
permalink: "/2010/09/03//"
---

As a human being you get value out of every heartbeat. Nutrients are transported out to every hungry cell in your body and at the same time waste products are returned providing feedback to regulation mechanisms. To increase the value provided by the heartbeat, the body can increase the frequency: with a more rapid pulse, nutrients reach their destination faster and feedback is coming back more quickly.

On your IT projects you get value out of every release cycle. New functionality allows users to be more productive and at the same time feedback about possible improvements will be sent back to the development team. One way to increase the value of your IT project is to shorten your development cycles. This will allow users to reap the productivity benefits faster and also provide earlier feedback about improvements allowing for cheaper changes.

Cutting release cycles down is only possible if you are able to decompose requirements and development tasks into very small parts. Requirements should be so small that a single developer can implement a couple of requirements every week. Development tasks should be so small that a single developer can finish several tasks every day.

Most people know how to decompose a big task into smaller tasks. But many people stop this decomposing process too early. To learn how to decompose into very small parts only requires you to keep decomposing. Try to practice this the next time you work on requirements or tasks.

Thanks to extensive decomposition, we have been doing weekly releases of the software for the Wizer project all since the project started two years ago providing tremendous value to the users.

Maybe your projects could benefit from more extensive decomposition?
