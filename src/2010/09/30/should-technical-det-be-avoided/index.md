---
date: 2010-09-30
layout: post-en
tags: post
title: Should technical debt be avoided?
author: Lars
categories: [thoughts, business]
permahost: "https://zealake.com"
permalink: "/2010/09/30/should-technical-det-be-avoided/"
---

We often use the term *technical debt* when referring to code  that development have currently postponed but will eventually have to complete at some point in the future. Examples can be a polished installer, a refactoring of the data access layer, an optimization of important queries, improved modularization, and known bugs.

When doing agile development technical debt is often seen as a problem because it prevents  us from shipping working software frequently. If we have a list of known bugs that must be fixed before we can ship, we don't really know when we can ship, because we don't know how long time it will take to fix those bugs.

When we conduct a technical due dilligence of a software company for a potential investor, one of the things we often look out for is the technical debt of their projects. Technical debt is a term that investors grasps intuitively, and it is often a good way to quantify how "bad" some situation is.

But as investors know very well, debt is not always a bad thing. On many projects time-to-market is incredibly important, so pushing some work into the future might give you a head start and the momentum that is critical for the success of the project.

It is therefor important to look at different types of technical debt, and I think it might be useful to categorize the different types into short-term and long-term debt.

Short-term debt is debt that you will have to pay before the next version is shipped. Bugs and bad performance that would kill your product is short-term debt. Since you will have to get rid of short-term debt before you ship you should really avoid postponing that sort of problems at all, see [Don't track bugs; fix them!](/2010/08/22/dont-track-bugs/).

Long-term debt is debt that might slow you down, but is not something you absolutely must pay before the next version is shipped. Architecture improvements are often on this list. Since a bad architecture probably slows down development it will have to be fixed eventually. But fixing it for this release might be more expensive than the savings it provides in this same release, and therefor it might make good economic sense to postpone the problem.

Technical debt, like all other debt, accumulate interest. The cost of a bad architecture increases as the project team is scaled up. At some point it makes more economic sense to fix the architecture problem to get rid of the debt.

So being aware of the tecnical debt of any project is always relevant. And not only for investors.
