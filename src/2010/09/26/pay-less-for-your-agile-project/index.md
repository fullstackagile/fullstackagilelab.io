---
date: 2010-09-26
layout: post-en
tags: post
title: Pay Less for your Agile Project
author: Lars
categories: [business]
permahost: "https://zealake.com"
permalink: "/2010/09/26/pay-less-for-your-agile-project/"
---

What is the right price for an agile project?

A traditional fixed price contract needs to be based on a detailed requirement specification. And that doesn't make sense for an agile project, where the ongoing establishment of requirements during the project is an important benefit.

Most commercial agile projects, therefor, is based on a time and material contract. But time and material contracts have drawbacks, especially for the customer. The supplier have no economic incentive for efficiency when working on a pure time and material contract. That might be the reason why many customers tell that agile projects gets more expensive than they expected.

At ZeaLake and BestBrains we have been using a new contract model that focus on collaboration and shared goals, which we call a Collaborative Contract.

The price model in a collaborative contract have two elements: a (low) hourly price and a completion price. The project is divided into subprojects with a duration of 1 to 2 months. Each subproject gets an individual completion price. The completion price is payed when the customer explicitly approves the delivered software - or implicitly approves it by putting the delivered software in production. In this way the price model becomes a fruitful combination of fixed price and time and material models.

![](./collaborative-contract.png)

Using this collaborative contract model both customer and supplier have an economic incentive to complete the project with as little work as possible. The customer will realize their business case faster and cheaper. The supplier will reap a higher profit per hour delivered.

We have had very good results from using this contract model on several projects during the last few years.
