---
date: 2010-08-22
layout: post-en
tags: post
title: Don't track bugs; fix them!
author: Lars
categories: [work process]
permahost: "https://zealake.com"
permalink: "/2010/08/22/dont-track-bugs/"
---

We got a question the other day about which bug tracking system we would recommend? After some discussion, I concluded that I wouldn't recommend any at all. Because I believe bugs shouldn't be tracked.

### Track systems are seductive

A bug tracking system feels so nice. When you discover an error or get a new idea, and you can't do anything about it right now, you can at least enter it into the system. You feel a personal satisfaction that you could do something useful, and it only took five minutes with no effort at all.

Because it's almost free to file a bug, bug tracking systems tend to pile up bug reports. 80% of the use of the system is input, and when it is actually used for serious output, the massive number of bug reports prevents your overview and is mostly a cause of frustration. We have seen several bug tracking systems containing many thousand bug reports.

The large number of bug reports hinders a clear overview and prioritization. You start out with a number of severety categories (e.g. 1 = serious, 2 = moderate, 3 = small /nice to have). Soon there are so many bug reports in category 1, that it seems clear that only category 1 reports will ever get fixed. And then it becomes important to distinguish between all the category 1 reports. So after a while the categories are supplemented by special lettering (making the title start with an *-mark), other attributes and new categories (0 = really, really serious and important bug).

### Bug tracking systems are symptoms of a heavy process

Using a bug tracking system reflects your expectation that you will have a lot of bugs to keep track of. However true from the beginning, this expectation often becomes self-fulfilling.

The urge to keep track of bugs arises in a development setting where you are not able to make the change in the software right away. If your team's development method is weighed down by heavy documentation, long release cycles, difficult prioritizing and long distance to the end user -- then the bug tracking system provides a feeling of having added at least a little value. In such a setting, a bug tracking system might be better than none. But I will rather recommend a more lean development process that renders the bug tracking system superfluous.

Entering a bug report adds no value to the software. Ideally, you should be able to start correcting the error as soon as you detect it. If you could fix it on the spot, you wouldn't waste time writing prose about it. If your process is sufficiently lean, you won't have many bugs. When you integrate and test continuously, and you have a close dialogue with your users, then errors will be found and corrected in very short cycles.

The lean, or agile, development approach focuses on finishing a piece of work in one coherent process. No waste, no stock, no waiting time. A bug tracking system is a stock of bugs (yuk!).

Note that having a system for handling customer requests is an entirely different matter, which I have no objections against. Customers will contact the support department with all kinds of enquiries about the software's behavior, and there is an obvious need to support the answering of those calls, but the large majority of these requests will not involve the development team, so the customer request system should not be used by the development team.

### What you really need is a piece of paper

When you follow a lean development process, you have a backlog of planned tasks that will be relatively short and surveyable, and ordered by importance. When you want to add a new task to the backlog, you need to consider its importance against the existing tasks. One possibility is that the new task will end up at the bottom of the backlog. Then your conclusion might be to just as well drop it, because the team will probably never reach the end of the backlog. Another possibility is that the new task is important enough to be entered around the top of the backlog. Then you need to decide which tasks it will push down the backlog, and assess the consequences. This pushing down of less important tasks ensures that adding a task makes you feel its cost.

Because the backlog never grows large, it can be kept in any kind of medium, for example in a text document or sticky notes on the wall. If you get more sticky notes than there is room for between the book case and the door opening, it's time to prioritize.

To conclude: Bugs should not be tracked: avoid them or fix them instead. Should I recommend a bug tracking (or backlog) system proper, it would most likely be a kind of spreadsheet tool with the feature of easily moving rows up and down for reprioritization, plus a system setting that limits the total number of rows allowed. Start setting that number to 25.

![](./bugbox.jpg)
