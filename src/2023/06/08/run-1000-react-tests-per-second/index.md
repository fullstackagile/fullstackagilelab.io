---
date: 2023-06-08
layout: post-en
tags: post
title: How to run 1000 React tests per second
author: Lars
categories: [tool, test, code]
permahost: "https://www.fullstackagile.eu"
permalink: "/2023/06/08/run-1000-react-tests-per-second/"
---

Presentation by Lars Thorup at Copenhagen React meetup June 8th, 2023.

[Video recording on Youtube](https://www.youtube.com/live/1DoUPZaUDl8?feature=share&t=2406)

![snail after running cheetah](./snail-cheetah-green.png)

- A couple of years ago, in a previous company of mine, we had *very* fast front-end tests
  - running 100s of tests per second
- These days, front-end tests tend to be slow
  - barely running 1 test per second 
- WTF?!?

Note: I had to write a follow-up blog post to this one: [We can still run 1000 React tests per second - reconciler edition](/2024/12/29/run-1000-react-tests-per-second-reconciler/) 

## How it started

![](./tz-mocha-enzyme.gif)

(1553 tests in 4 seconds - with Mocha and Enzyme)

## How it's going

![](./invert-vitest-rtl-jsdom.gif)

(89 tests in 80 seconds - with Vitest, React Testing Library and jsdom)

## So: what's the problem?

- It's not just Jest (although [Vitest](https://vitest.dev/) *is* faster)
- It's not just React Testing Library
- It's primarily the DOM
  - and yes: jsdom is *even slower* than a real browser

![DOM testing profile](./dom-testing-profile.png)

## Prefer unit tests over integration tests

![Testing Pyramid](./test-automation-pyramid-640x586.jpg)

- How to write unit tests for the front-end?
- Exclude the DOM from testing
- Focus on testing our own code
- Interact directly with React, our components and the React element tree
- Keep a few integration tests for interesting DOM integration

## Introducing "react-test-renderer"

- [react-test-renderer](https://github.com/facebook/react/tree/main/packages/react-test-renderer)
- Like React, it is built by Facebook
- Shipped as part of React
- Includes element queries, like `findByProps` and `findByType`, like React Testing Library
- Runs 1000s of tests per second
- Demo in [react-test-renderer-sandbox](https://github.com/larsthorup/react-test-renderer-sandbox/) GitHub repo

![react-test-renderer-sandbox sub millisecond test runs](./react-test-renderer-sandbox.png)


## Simple example test

```jsx
function Count() {
  const [count, setCount] = useState(0);
  const onClick = () => {
    setCount((count) => count + 1);
  };
  return (
    <button onClick={onClick}>
      <span>{`count is ${count}`}</span>
    </button>
  );
}

it("should let user click to increment count", () => {
  const { root } = TestRenderer.create(<Count />);
  const button = root.findByType("button");

  button.props.onClick();

  expect(button.props.children.props.children).toEqual("count is 1");
});
```

## Comparison

- Comparing react-test-renderer against React Testing Library
- Sample app, simple DOM, realistic user flow
- RTR: [App.react.test.tsx](https://github.com/larsthorup/react-redux-typescript-vite-sandbox/blob/main/app/src/view/App.react.test.tsx) - split into a test per page
- RTL: [App.dom.test.tsx](https://github.com/larsthorup/react-redux-typescript-vite-sandbox/blob/main/app/src/view/App.dom.test.tsx) - a single test
- RTR is 50-150 times faster than RTL

![150 times faster on Windows](./RTR-RTL-comparison-windows.png)
![50 times faster on Ubuntu](./RTR-RTL-comparison-ubuntu.png)

## Trade-offs

- Must invoke component event handlers directly
  - no "DOM" events or "user-event" package
- Must mock various DOM features that our code might depend on
  - like `addEventListener`, `getElementById`, `scrollTo` etc

## Pursue the speed!

![1500 dots in 4 seconds](./app.react.test.gif)

(1500 tests in 4 seconds - with Vitest and React Test Renderer)
