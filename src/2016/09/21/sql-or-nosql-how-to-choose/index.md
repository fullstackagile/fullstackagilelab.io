---
date: 2016-09-21
layout: post-en
tags: post
title: SQL or NoSQL - how to choose?
author: Lars
categories: [talk, decision]
permahost: "https://zealake.com"
permalink: "/2016/09/21/sql-or-nosql-how-to-choose/"
---

Yesterday I gave a talk on the topic of SQL and NoSQL database, how to choose between them, and a demonstration of Redis. Here is the appetizer for the talk:

<a href="http://www.slideshare.net/larsthorup/sql-og-nosql-how-to-choose" target="_blank">

![sql-nosql](./sql-nosql-300x226.png)

</a>

The past years, a number of new database systems have appeared, like MongoDB and Redis. Most of them have radically new ways to look at data persistance, where efficient replication is prioritized over advanced query support.

In this talk we will discuss some of the benefits and drawbacks of the new key/value stores and document databases. As an example, we will demonstrate Redis, an advanced key/value store. Redis is different from most other key/value stores on two dimensions: It runs entirely in RAM and it supports a number of advanced data structures with accompanying specialized algorithms.

And here are the [slides](http://www.slideshare.net/larsthorup/sql-og-nosql-how-to-choose).
