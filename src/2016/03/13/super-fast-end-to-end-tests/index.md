---
date: 2016-03-13
layout: post-en
tags: post
title: Super fast end-to-end tests
author: Lars
categories: [talk, testing]
permahost: "https://zealake.com"
permalink: "/2016/03/13/super-fast-end-to-end-tests/"
---

Automated end-to-end tests are often seen as a necessary evil. A common example is Selenium-based browser tests.

This kind of testing has many drawbacks:

* They take a long time to run
* They require complicated setup
* They are fragile

But it doesn't have to be like that!

In this talk I describe how we can write automated end-to-end tests that are

* Superfast
* As easy to setup as unit tests
* As robust as unit tests

This technique is leveraging existing unit testing techniques. Tests for lower layers (such as server code) is instrumented to record all requests and responses in a log file. Tests for higher layers (like client code) is extended with a mocking layer that automatically configures with the contents of that log file.

It is then possible to run almost a hundred end-to-end tests every second.

Read my blog post with more details [here](/2015/01/05/unit-test-your-service-integration-layer/).

Here are my [slides](https://www.slideshare.net/larsthorup/fast-endtoendtests-59497903) from this presentation, that I gave the other day. 
