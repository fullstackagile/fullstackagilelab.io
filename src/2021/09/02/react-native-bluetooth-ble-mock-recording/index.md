---
date: 2021-09-02
layout: post-en
tags: post
title: Sub-second integration tests for your React Native app and Bluetooth device
author: Lars
categories: [test, talk]
permahost: "https://www.fullstackagile.eu"
permalink: "/2021/09/02/react-native-bluetooth-ble-mock-recording/"
twitterlink: "/2021/09/02/react-native-bluetooth-ble-mock-recording"
---

Today I gave a talk at React Native EU:

> This talk is targeted developers creating apps for a Bluetooth device, such as a loudspeaker, a
> toothbrush or a dishwasher. I report on my experience using the technique of "mock recording"
> to get very fast and robust integration tests.
> In collaboration with SOUNDBOKS, a Bluetooth speaker company, I have developed an open
> source tool for creating and using recordings of Bluetooth (BLE) traffic to test a React Native
> app using Jest. The tool makes it possible to run several integration tests per second as
> opposed to several minutes per end-to-end test. The tool is based on years of production
> experience using mock recording for web traffic.
> In this talk I introduce the methodology, perform a live demonstration of the tool, and report on
> our experience using the tool during app development: how is the quality and speed of
> feedback from these tests, and how easy is the tool to use for developers?
> The tool is available at https://www.npmjs.com/package/react-native-ble-plx-mock-recorder.

## Video

[![Video on Youtube](./react-native-2021-lars-youtube-cover.png)](https://www.youtube.com/watch?v=LKj2CCfxX8A)

## Slides

[![](./jest-android-bluetooth.png)](./bluetooth-mock-recording-reactnativeeu-2021-09-02-publish.pdf)
[Slides](./bluetooth-mock-recording-reactnativeeu-2021-09-02-publish.pdf)
