---
date: 2013-08-18
layout: post-en
tags: post
title: Database Schema Evolution
author: Lars
categories: [talk, architecture, database]
permahost: "https://zealake.com"
permalink: "/2013/08/18/database-schema-evolution/"
---

I recently gave a presentation for a client on how to manage database changes in a large development organization with multiple database systems, multiple databases and multiple applications. I talked about how you can streamline your database changes by versioning your database instances and your database schema, running database instances locally and implementing database changes using migration scripts based on database refactoring patterns.

Read the slides here:

<a href="https://www.slideshare.net/larsthorup/database-schema-evolution" target="_blank">

![](database-schema-evolution.png)

</a>
