---
date: 2013-07-09
layout: post-en
tags: post
title: Advanced Jasmine - Front-End JavaScript Unit Testing
author: Lars
categories: [talk, testing, UI]
permahost: "https://zealake.com"
permalink: "/2013/07/09/advanced-jasmine-front-end-javascript-unit-testing/"
---

Unit testing front-end JavaScript presents its own unique set of challenges. Yesterday we had another event in the Test Driven Developers Bay Area meetup group. At this event I presented a number of different techniques to tackle these challenges and make our JavaScript unit tests fast and robust using Jasmine ([another version using QUnit](/2013/11/14/advanced-qunit-front-end-javascript-unit-testing/)). I covered the following subjects:

* Mocking and spy techniques to avoid dependencies on
  * Functions, methods and constructor functions
  * Time (new Date())
  * Timers (setTimeout, setInterval)
  * Ajax requests
  * The DOM
  * Events
* Expressive matchers
  * jasmine-jquery
* Structuring tests for reuse and readability
* Testing browser-specific behaviour

You can watch the 1:30 hour [video](https://www.youtube.com/watch?v=g4eQplHxU18), just hear the [audio](https://www.youtube.com/watch?v=8FUwc3gZDMw) (better sound), look at the [code](https://github.com/larsthorup/jasmine-demo-advanced) and watch the slides:

<a href="http://www.slideshare.net/larsthorup/advanced-jasmine" target="_blank">

![](advanced-jasmine-frontend-javascript-unit-testing-1-638.jpg)

</a>
