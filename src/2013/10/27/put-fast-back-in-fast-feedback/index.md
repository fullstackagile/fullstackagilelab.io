---
date: 2013-10-27
layout: post-en
tags: post
title: Put "fast" back in "fast feedback"
author: Lars
categories: [talk, KPIs]
permahost: "https://zealake.com"
permalink: "/2013/10/27/put-fast-back-in-fast-feedback/"
---

I recently gave a presentation at a meetup in Pleasanton, California on fast feedback. One of the cornerstones in Agile development is fast feedback. For engineering, "fast" means "instantly" or "in 5 minutes", not "tomorrow" or "this week". Your engineering practices should ensure that you can answer yes to most of the following questions:

* Do we get all test results in less than 5 minutes after a commit?
* Is our code coverage more than 75% for both front-end and back-end?
* Can we start exploratory testing in less than 15 minutes after a commit?
* Do all our tests pass more than 90% of our commits?

This talk will give you practical advice on how to get to "yes, we get fast feedback".

Read the slides here:

<a href="https://www.slideshare.net/larsthorup/2013-10zealakefastfeedback" target="_blank">

![](put-fast-back-in-fast-feedback-1-638.jpg)

</a>
