---
date: 2013-11-14
layout: post-en
tags: post
title: Advanced QUnit - Front-End JavaScript Unit Testing
author: Lars
categories: [testing, UI]
permahost: "https://zealake.com"
permalink: "/2013/11/14/advanced-qunit-front-end-javascript-unit-testing/"
---

A couple of months ago I linked to a presentation on [advanced unit testing techniques for front-end JavaScript using Jasmine](/2013/07/09/advanced-jasmine-front-end-javascript-unit-testing/). This week I did a presentation for a client using QUnit instead, covering roughly the same subjects:

* Mocking and spy techniques to avoid dependencies on
  * Functions, methods and constructor functions
  * Time (new Date())
  * Timers (setTimeout, setInterval)
  * Ajax requests
  * The DOM
  * Events
* Structuring tests for reuse and readability
* Testing browser-specific behaviour
* Leak detection

You can look at the [code](https://github.com/larsthorup/qunit-demo-advanced) and watch the slides:

<a href="https://www.slideshare.net/larsthorup/qunit-demoadvancedslides" target="_blank">

![](./advanced-qunit-frontend-javascript-unit-testing-1-638.jpg)

</a>
