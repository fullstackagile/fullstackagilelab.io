---
layout: page-en
permalink: "/larsthorup/"
title: Lars Thorup
---

# Lars Thorup

<img src="../lars.jpg" width="18%"/>

## Profile

I am an experienced JavaScript, Python and SQL software engineer, practicing and coaching Test Driven Development and Continuous Delivery. I aim for robust design, fast feedback and clean code. Frequently delivering important features in high quality to real users is what motivates me most.

- Roles: software engineering, software architecture, test automation, DevOps
- Location: Copenhagen, Denmark
- GitHub: [larsthorup](https://github.com/larsthorup)
- Twitter: [larsthorup](https://twitter.com/larsthorup)
- LinkedIn: [larsthorup](https://linkedin.com/in/larsthorup)
- Blog: [fullstackagile](/blog)

## Skills

- Front-end: React, Redux, TypeScript
- Mobile: React Native, Bluetooth BLE
- Back-end: Node.js, Python
- Persistence: PostgreSQL, SQL Server
- Infrastructure: AWS, Terraform
- Delivery: Bash, GitLab, GitHub

## Experience

### DTU

January 2023 - present, Lyngby, Denmark, part-time

Teaching a course in front-end web technology: TypeScript, React, CSS

### Invert Bio

April 2022 - present, Copenhagen, Denmark, Freelance

Full-stack web development: TypeScript, React, Redux, Python, MongoDB

### ZeroNorth

October 2021 - March 2022, Copenhagen, Denmark, Freelance

Full-stack web development: TypeScript, React, Redux, PWA, Python, AWS, Terraform

### SOUNDBOKS

November 2020 - Sepember 2021, Copenhagen, Denmark, Freelance

End-to-end test automation, TypeScript, React Native, Bluetooth

### BASE Life Science

January 2020 - March 2021, Lyngby, Denmark, Freelance

Lead developer, TypeScript, Python, React, Redux

### Nets

October 2019 - January 2020, Copenhagen, Denmark, Freelance

Test automation, Java, TypeScript, Angular, HoverFly

### Triggerz

August 2016 - December 2019, Copenhagen, Denmark, Co-founder & CTO

Architect, lead developer, team lead, Continuous Deployment, React, Redux, Node.js, PostgreSQL

### BTS

August 2008 - August 2016, Copenhagen & San Francisco, Freelance

Lead Developer, Architect, Team Lead. Visualisation software for real time feedback from conference participants. Continuous Delivery. C#, JavaScript, SQL Server.

### ZeaLake

October 2010 - December 2015, California, Founder

Software consulting, technical due dilligence

### Staance

February 2015 - September 2015, San Francisco, Freelance

Full-stack developer. Node.js, Redis, JavaScript

### Apple

February 2013 - December 2013, California, Freelance

store.apple.com. JavaScript, Jasmine

### Alteryx

October 2011 - February 2013, Colorado, Freelance

Continuous Delivery. C#, JavaScript

### BestBrains

April 2001 - December 2011, Copenhagen, Denmark, Founder

Software consulting, technical due dilligence

### Novo Nordisk

June 2007 - June 2008, Denmark, Freelance

Embedded software for an insulin device. C++, C. Test software for the device in JavaScript, .NET.

### Mutual Satellites

2006 - 2007, Denmark, Founder

Model-Driven Development, C#, SQL Server

### Adomo

2000 - 2001, Denmark & California, Employee

C, C++, Java, PostgreSQL

### Catalog International

1999 - 2000, Denmark, Employee

C++

### ScanJour

1994 - 1999, Denmark, Employee

C++

## Education

1984 - 1993, Master of Computer Science, University of Copenhagen
