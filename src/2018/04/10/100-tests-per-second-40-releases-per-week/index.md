---
date: 2018-04-10
layout: post-en
tags: post
title: 100 tests per second – 40 releases per week
author: Lars
categories: [talk, delivery]
permahost: "https://zealake.com"
permalink: "/2018/04/10/100-tests-per-second-40-releases-per-week/"
---

In April 2018 I gave a talk where I showed how the [Triggerz](https://www.triggerz.com/) engineering team continuously deliver new software versions to our users.

The Triggerz product is a web application built with React, Node.js and PostgreSQL. The product has been live since 2017 with users worldwide.

We have built a simple continuous deployment pipeline, also mostly in JavaScript, that we use to validate every push to master before deploying it automatically to production.

* [Video](https://www.youtube.com/watch?v=Iuc34vqVz2E)
* [Slides](https://www.slideshare.net/larsthorup/100-tests-per-second-40-releases-per-week)
* [Blog post about how to do fast integration tests](/2015/01/05/unit-test-your-service-integration-layer/)
