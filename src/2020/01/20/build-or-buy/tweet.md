---
date: 2020-01-31
draft: true
layout: post-en
tags: post
title: Choose your own route!
permahost: "https://www.fullstackagile.eu"
permalink: "/2020/01/31/build-or-buy/tweet/"
---

# Tweets for Build or buy?


---

Build or buy? You probably discuss this frequently on new projects: Should we reuse an existing LibX or write our own? How do we decide? 

Here are 6 useful criteria for making the choice: (thread below, blog post here: https://www.fullstackagile.eu/2020/01/20/build-or-buy/

[//]: # (TODO picture)

---

1/6: Is LibX widely used? 

There will be lots of community support and it will be much easier to find developers who already know this library. Widely used libraries also tend to work well with other widely used libraries. 🌍

---

2/6: Is LibX high quality?
 
Is the library well-covered by automated tests? Is it well-documented? How good is the support provided by the authors? Is LibX free of serious bugs? Is the API robust and reasonably stable? ❤️

---

3/6: Does LibX handle all our needs? 

Can the library handle not just our current needs, but also potential future needs? And if LibX is missing some tiny feature we need, we might choose to fork LibX, and add the feature we need. 🌈

---

4/6: Do we need all of LibX? 

Superfluous functionality often comes with costs: increased code complexity, slower performance, slower bug fixing and further development, increased cognitive overhead. 💸

---

5/6: Does LibX fit our paradigm? 

Do we follow a stateful, an immutable, or a reactive paradigm? Do we have centralized or distributed state management? Are points in time represented as unix time, Date objects, ISO-8601 strings? 🕒  

---

6/6: Do we have the skills to write our own? 

Creating a simple API, staying free of bugs, getting potentially tricky functionality implemented correctly for the myriad known and unforeseen circumstances is hard. Know your own limitations here! 😎

---

Build or buy? Discuss with your team how to weigh the different criterias relative to each other for your particular project. 

Read the full blog post with lots of examples here: https://www.fullstackagile.eu/2020/01/20/build-or-buy/
