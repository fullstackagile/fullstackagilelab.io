---
date: 2020-01-31
layout: post-en
tags: post
title: A brief intro to using a kanban board
author: Sju
categories: [guide, work process]
permahost: "https://www.fullstackagile.eu"
permalink: "/2020/01/31/kanban/"
---

I got a question from a colleague the other day, asking me for a short motivation of the use of a kanban board. The context for her question was not software development, but rather a marketing department, so the usual software-oriented introductions wouldn't work well. Instead I wrote this introduction:

### Why use a kanban board

The primary driver for using a kanban board is **transparency**, especially from an Agile perspective. With a kanban board we can discuss and communicate what is the current priority (cards at the top of the board), what we are currently working on (cards in the "in progress" column), who is working on what (mentioned on each card), and what the next tasks will be.

This transparency supports the team in doing things that are usually difficult. Clear **prioritization** and thus de-prioritization (spells out to management that everything cannot be of the same importance), **focus** (one person should not be mentioned on many "in progress" cards), **frequent delivery of value** (pull cards into "done" to avoid overcrowding "in progress"). Fast and frequent delivery of value will then provide fast and frequent **feedback**, which will then improve the **quality** of prioritization and of the delivery of future tasks.

### How to use a kanban board

Wikipedia has a good introduction to the use of a kanban board: [https://en.wikipedia.org/wiki/Kanban_board](https://en.wikipedia.org/wiki/Kanban_board)

This blog post explains how kanban can work for marketing teams: [https://business901.com/blog1/why-you-should-use-kanban-in-marketing/](https://business901.com/blog1/why-you-should-use-kanban-in-marketing/). The blog, Business 901 / Lean Marketing Lab, covers many aspects of marketing work from a Lean/Agile perspective.

A few remarks about the kanban board as such:

Ideally the team is co-located and will use a real physical board with paper cards on a visible wall. When people can hold the actual card in their hand and everyone can see where you point, the process becomes more engaging, and thereby efficient. If the team is distributed, it might (but does not have to) be more helpful to use a virtual board. In such cases, I often recommend [Trello](https://trello.com/).

It should be up to the team to decide what specific columns to include on the board. They will start with whatever appears reasonable, and then reflect on how the board and the process works for them on a regularly basis: Should we do standups around the board more or less frequently? Should we introduce an "Awaiting" column? An "Urgent" lane? etc.

Did I forget any important parts of the "Why"? Please let me know.

![](./agile-team-at-task-board.jpg)
