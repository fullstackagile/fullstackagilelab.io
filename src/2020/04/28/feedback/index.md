---
date: 2020-04-28
layout: post-en
tags: post
title: Feedback increases both quality and team spirit
author: Sju
categories: [guide, collaboration]
permahost: "https://www.fullstackagile.eu"
permalink: "/2020/04/28/feedback/"
---

<div class="callout">
<img src="./woman-in-black-blazer-laptop-laughing-cropped.jpg" />
<div align="right"><small>Photo by <a href="https://www.pexels.com/@shvetsa" target="_blank">Anna Shvets</a></small></div>
</div>

When working together in a physical location, you often get informal feedback. You may not think of it as such, but a brief exchange across the desk or at the coffee machine, concluding with your colleague saying: “Sounds good”, is an indicator that you are on the right track. But when you don’t share a physical location, a lot of the things that would normally happen implicitly must be made explicit. This means a distributed team should work systematically with giving and receiving feedback.

Receiving feedback during a work process is of great value. You can catch omissions and misunderstandings early, saving time and effort at later stages. Thus your work products will be of higher quality and may even be delivered early. On top of that, a culture of mutual feedback has the extra benefit of keeping colleagues informed about each others’ ongoing work. This knowledge sharing can increase the team’s overall quality and effectiveness as well as the team spirit.

### Ask for feedback in a productive way

To benefit from others’ feedback, you must be able to endure their scrutiny of your unfinished thoughts and partial results. This exposure may invoke an uncomfortable feeling of vulnerability.

You can practice by asking for feedback on a small work item. Start by asking just one colleague for some specific sort of feedback. The person need not be an expert in the field – you can get very helpful feedback from colleagues at your own level. Even junior colleagues, bringing fresh eyes on the matter, can often contribute valuable insight.

Ask for feedback to the specific aspect that is most in need for improvement. When you send your draft, don’t just write ”Your comments are welcome”. Be specific: “I have been deliberating ... and decided on ... How do you think that works?” 

### Give feedback for mutual benefit

To give feeedback can feel difficult too. You may be reluctant to offend your colleague or hurt their feelings, or you may be unsure what kind of feedback they want. In that case: ask, so you can spend your efforts on exactly the aspects they need.

Giving productive feedback is a skill everyone should develop continuously. Start out on a positive note before you provide your suggestions. A positiv starting point will give the recipient a ballast for taking in the items you point out to improve. The effort of selecting and phrasing your positive introduction will also help you set a constructive frame of mind: ”I think your choice of perspective is really good.”

When discussing your feedback, call it considerations or improvement suggestions. Assume that your colleague has good reasons for what they created. Use respectful phrases such as “What assumption did you make about …” and “I think the topic of … could be covered more in-depth.”

Remember to prioritize your feedback. You may have found 25 improvement items, but in general the recipient can’t embrace more than a handful of items, unless they specifically asked for a proofreading. Select the few most important suggestions that your colleague realistically can incorporate, given their competence and available time and resources. Be as precise as possible when you give feedback – when you praise as well as criticize. Very general statements are difficult to act on and learn from.

When you give critical feedback, do it by phone or on a personal chat channel. Your recipient is much better able to take in your input if it happens in a confidential space. Conversely, when you give positive feedback preferably involve an audience, e.g. reply-all to the email or in the shared chat channel: ”I think your presentation is brilliant. Brief and clear! I love your example of ...” Hearing positive feedback contributes to the team spirit and sparks others’ interest in asking for feedback.

### Receive feedback constructively

Receiving feedback in a constructive and matter-of-fact way requires practice. Don’t spend any effort on defending your draft. Maintain the presumption that the two of you are discussing a work product from your team or department – not bestowing a exam grade for you personally. You and the person giving feedback have a shared interest in a good work product, and in both of you broadening your knowledge.

Make sure that you understand what your colleague means by their feedback. Ask questions: “So do you feel this choice of words is confusing in this paragraph, or throughout the text?” Acknowledge the input: “Yes, I can see that. I will try to rephrase it.” When you end the conversation, thank your colleague for their feedback.

Some of the feedback may not be quite relevant or useful, and that is perfectly okay. The purpose of the feedback interaction is not to reach an agreement. The final product is your responsibility and you make the decisions about what parts of the feedback to put to use.

When you receive feedback in a constructive way, your colleague will be encouraged to participate another time. This will increase the motivation for both of you.

<div class="callout border">

Try today: 

* Pick a partial result of your work today that you feel may be improved. 
* Ask a colleague for feedback on it. 
* Notice how your interaction works out: Should you have asked for more specific feedback? Ought you to have set a deadline? Would the feedback  have been better given in a conversation than in writing? Did your colleague know enough of the context to give helpful feedback? 
* Based on this experience, prepare how you will ask another colleague for feedback tomorrow. 

Enjoy!

</div>

### Get going right away

Some feedback can be given in writing, mainly minor and uncontroversial items. More profound or comprehensive suggestions for improvement should be given in an oral conversation to make sure you have a shared understanding.

When working remotely, you can’t easily figure out if you are interrupting your colleague. Whether you are asking for or giving feedback: if you see the need for a conversation, then make an appointment for the phone call to make sure the time suits both of you.

A good feedback culture is of great value for any team, and even more important for distributed teams. Fortunately you don’t need to wait – a single individual can start increasing the feedback culture in their team or department right away.
