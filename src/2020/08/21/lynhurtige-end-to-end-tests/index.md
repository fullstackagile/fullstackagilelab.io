---
date: 2020-08-21
layout: post-en
tags: post
title: Lynhurtige end-to-end tests - foredrag (in Danish) 
author: Lars
categories: [test,talk]
permahost: "https://www.fullstackagile.eu"
permalink: "/2020/08/21/lynhurtige-end-to-end-tests/"
---

Hvordan kan man køre 100-vis af robuste end-to-end tests per sekund? Se mit foredrag som jeg holdt hos [Lund&Bendsen](https://logb.dk/) på Youtube herunder,  og download mine slides.

## Video (på dansk)

<iframe width="560" height="315" src="https://www.youtube.com/embed/AuzwYirt8eA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Slides (in English)

[![](./cheetah-green.png)](./fast-end-to-end-tests.pdf)
[Slides](./fast-end-to-end-tests.pdf)
