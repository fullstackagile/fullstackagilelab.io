---
date: 2024-12-29
layout: post-en
tags: post
title: We can still run 1000 React tests per second - reconciler edition
author: Lars
categories: [tool, test, code]
permahost: "https://www.fullstackagile.eu"
permalink: "/2024/12/29/run-1000-react-tests-per-second-reconciler/"
---

This post is a follow-up to a post of mine from last year: [How to run 1000 React tests per second](/2023/06/08/run-1000-react-tests-per-second/). I suggest that you take a minute to read it first.

Since then the React team have decided to deprecate the [`react-test-renderer`](https://www.npmjs.com/package/react-test-renderer) package, so while we can still use it with `react@19`, I expect it to no longer be available with the next major upgrade of React.

Fortunately there is another way, so we can still run 1000s of React tests per second.

![](./aa-reconcile.jpg)

Conceptually the old `react-test-renderer` package was a "custom" React renderer. For quite some time, the React team has exposed an API, [`react-reconciler`](https://www.npmjs.com/package/react-reconciler) that allows users to build their own custom React renderer. And quite a few such renderers already exists, including the two well-known renderers from the React team itself: React DOM and React Native. Other popular custom renderers include:
- [`react-three-fiber` for Three.js](https://www.npmjs.com/package/@react-three/fiber)
- [`@react-pdf/renderer` for PDFs](https://www.npmjs.com/package/@react-pdf/renderer)

While `react-test-renderer` never appeared to gain much traction, `react-reconciler` appears to be much more well-established in the eco-system, also by the React team itself. However, the API of `react-reconciler` is still marked as "experimental", and thus can be expected to change quite a bit between React releases.

Nevertheless, it is quite easy to build your own custom test renderer on top of the `react-reconciler` package. A test renderer can emit a simple object structure with `type`, `props` and `children` to represent the output. Tests can then interact with this object structure, assert on the presence of nodes, and interact with event handlers found on `props`. Such tests are much faster than rendering to a real DOM (or jsdom).

To demonstrate the performance, I have extended my `react-test-renderer-sandbox` GitHub repo with an implementation based on `react-reconciler`, running ~3000 tests per second in GitHub Actions:

[![~3000 React tests per second](../react-test-renderer-sandbox@590ba9c.png)](https://github.com/larsthorup/react-test-renderer-sandbox/blob/main/README.md)

The React component being tested above is quite simple, and does not involve any routing, state management or complex HTML. For a more realistic example, I migrated the tests on another of my projects, [react-redux-typescript-vite-sandbox](https://github.com/larsthorup/react-redux-typescript-vite-sandbox), and here we see it running 1500 tests in 1.47 seconds on a Mac M1:

![](./App.react.test.tsx.gif)

I have also extracted the code into its own NPM package: [@larsthorup/react-test-renderer](https://www.npmjs.com/package/@larsthorup/react-test-renderer) to make it easy for you to try it out on your own projects.
