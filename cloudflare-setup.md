# Cloudflare configuration

## Nameservers

* Follow Cloudflare instructions

## SSL

* Flexible
* Always use HTTPS

## DNS

* CNAME www - www.fullstackagile.eu.s3-website.eu-central-1.amazonaws.com
