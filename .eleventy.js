const syntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight");

module.exports = function (eleventyConfig) {
  eleventyConfig.addPlugin(syntaxHighlight);
  eleventyConfig.addCollection('postRecent', collection => {
    // Note: work around limitation in the liquid template language
    return collection
      .getFilteredByTag('post')
      .filter(p => !p.data.draft)
      .reverse();
  });
  eleventyConfig.addPassthroughCopy('src/style');
  eleventyConfig.setBrowserSyncConfig({
    open: 'local'
  });
  return {
    templateFormats: [
      'gif', // Note: uses passthroughFileCopy
      'html',
      'jpg', // Note: uses passthroughFileCopy
      'md',
      'pdf',
      'png', // Note: uses passthroughFileCopy
    ],
    dir: {
      input: 'src',
      output: 'public'
    },
    passthroughFileCopy: true
  };
};
